import { Meteor } from 'meteor/meteor'

export default {
  loadStages,
  changeStages
}

async function loadStages() {
  return new Promise((resolve, reject) =>
    Meteor.call('fetchStages', (err, stages) => {
      if (err) {
        reject(err)
      } else {
        resolve(stages)
      }
    })
  )
}

async function changeStages(changes) {
  return new Promise((resolve, reject) => {
    Meteor.call('changeStages', changes, (err, newStageIds) => {
      if (err) {
        reject(err)
      } else {
        resolve(newStageIds)
      }
    })
  })
}
