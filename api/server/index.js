import { Stages } from './stages'

export default {
  fetchStages
}

function fetchStages() {
  return Stages.find({}, { sort: { number: 1 } }).fetch()
}
