import { Template } from 'meteor/templating'
import './sortable-list'
import './stage-list-item'
import './stage-list-controls'
import './stage-list.html'

const t = Template.stageList

t.helpers({
  listIsReady() {
    return this.list.isReady()
  },
  uiOptions() {
    return {
      handle: '.drag-handle'
    }
  }
})
