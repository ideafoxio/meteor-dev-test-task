import { Template } from 'meteor/templating'
import API from '/api/client'
import './stage-list-controls.html'

const t = Template.stageListControls

t.onCreated(function() {
  this.handleRevert = () => this.data.list.reset()
  this.handleSave = async () => {
    // TODO: save new stage even if 'Add' button is not clicked
    const { list } = this.data
    try {
      list.lock()
      const changes = list.getChanges()
      const newStageIds = await API.changeStages(changes)
      this.setAddedItemsStageIds(newStageIds)
      list.setCurrentStateAsInitial()
    } catch (err) {
      console.error(err)
    } finally {
      list.unlock()
    }
  }

  this.setAddedItemsStageIds = newStageIds => {
    if (newStageIds && newStageIds.length > 0) {
      this.data.list
        .getAddedItems()
        .forEach((item, index) => (item.stageId = newStageIds[index]))
    }
  }
})

t.events({
  'click .js-revert'(evt, instance) {
    evt.preventDefault()
    instance.handleRevert()
  },
  'click .js-save'(evt, instance) {
    evt.preventDefault()
    instance.handleSave()
  }
})
