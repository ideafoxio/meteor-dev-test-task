# Test task for a Full-stack developer position at IdeaFox

The task is designed to challenge your skills required for day to day activities in IdeaFox and to make you familiar with our current tech stack. That is why you need to implement the solution on your own. It's absolutely fine to use external resources or discuss parts of the task with someone else but please do not distribute or share full content of the task.

## Challenge description

The project contains a simplified version of the stages configuration page with some functionality removed. The page allows the user to configure multiple stages for project and reorder them with Drag-and-Drop.

Your goal is to improve the page behavior as follows:

1. Update styling according to [wireframes](https://www.figma.com/file/wEy6N6C9W4fMEYgYh6jjMg/app-test?node-id=0%3A1)

2. Implement UX improvements:

   1. Save last item when user forgets to click `Add` button before saving list changes.
   2. Allow reverting changes for single item in the list.
   3. Update list elements styling based on the list state. E.g. enable buttons only when changes available, etc.

3. Implement server side method for saving changes into database. Test implemented solution with integration tests for the most critical scenarios.

### Done when

1. List changes are saved to the server.
2. UI is styled according to wireframes.
3. Last item is saved without hitting `Add` button.
4. Changes can be reverted for a single list item.

## Getting started

1. Fork the repository as you won't have write permissions for this repository.
2. Install [meteor](https://www.meteor.com/install).
3. Clone the forked repository.
4. `cd projectFolder`
5. `meteor npm install`
6. `npm start`

## Submitting the results

- Push the solution to the forked repository.
- Do not squash the commits but feel free to order them according to your preference.
- If required add comments in the codebase or create `Solution.md` file with all the comments and assumptions you feel necessary to implement the task.
- Do not create a PR to the main repository.
- Let us know once the task is finished and we can check the results.

## Contacts

If you have any questions regarding the test task send it to [vasily.belolapotkov@ideafox.io](mailto:vasily.belolapotkov@ideafox.io)
