import { Meteor } from 'meteor/meteor'
import API from '/api/server'

Meteor.methods({
  fetchStages() {
    return API.fetchStages()
  },
  changeStages() {
    // TODO: Implement and test method for saving changes
    throw new Meteor.Error(503, 'NOT_IMPLEMENTED')
  }
})
